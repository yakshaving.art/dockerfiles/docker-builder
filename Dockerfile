# This is a multistage build. In order not to install docker from scratch, we just
# piggyback on latest image, thus having the best of both worlds.
# hadolint ignore=DL3007
FROM docker:latest AS installer

# This is our final image, based on base image
FROM registry.gitlab.com/yakshaving.art/dockerfiles/base:master
LABEL maintaner="Ilya A. Frolov <if+gitlab@solas.is>" \
      description="Image we use as a builder image for other images"

# This is a multiarch image
ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-eufo", "pipefail", "-c"]

# Piggyback on docker:latest -- grab the binary and modprobe hack
COPY --from=installer --chown=root:root \
	/usr/local/bin/docker* \
	/usr/local/bin/modprobe \
	/usr/local/bin/

# hadolint ignore=DL3017,DL3018
RUN \
	# We bind-mount scripts and tests so that we don't pollute the end image
	# NOTE: instead of mounting separate dirs, we mount both so that we don't have
	# empty /mnt/scripts and /mnt/tests directory leftovers in the image :ocd:
	--mount=type=bind,source=.gitlab.d/ci,target=/mnt,readonly \
	# And we also bind-mount .trivyignore file
	--mount=type=bind,source=.trivyignore,target=/.trivyignore,readonly \
	# normal flow
	apk -qq --no-cache upgrade \
	# add binutils for stripping binaries
	&& apk -qq --no-cache add \
		binutils \
		coreutils \
		parallel \
		skopeo \
	# install binaries from hoarder, multiarch
	&& bash /usr/local/bin/unhoard.sh docker-buildx "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh goss "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh hadolint "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh shellcheck "${TARGETOS}" "${TARGETARCH}" \
	&& bash /usr/local/bin/unhoard.sh trivy "${TARGETOS}" "${TARGETARCH}" \
	# move buildx to proper location
	&& mkdir -p "/usr/local/lib/docker/cli-plugins" \
	&& mv /usr/local/bin/docker-buildx /usr/local/lib/docker/cli-plugins/ \
	# fix permissions
	&& ( set +f; chown root:root /usr/local/bin/* \
		&& chmod 0755 /usr/local/bin/* ) \
	# install other things below
	# remove binutils
	&& apk -qq del binutils \
	# at this point, the _build_ is done, and we proceed to run _tests_ from within the image,
	# in order to abort the whole thing if they fail, and never push anything unsafe to the registry
	# these stages should be the last, and they should also be self-contained, i.e. do not install
	# any dependencies to not pollute the end image
	&& bash /mnt/scripts/skkrty_inside_build.sh \
	&& bash /mnt/scripts/tests_inside_build.sh \
	&& echo "done"

# mimic docker:latest image
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["bash"]
