# docker-builder

This is an image to build other images and itself.

All CI scripts are locally executable, so you should be able to just run
`echo .gitlab.d/ci/scripts/{check,build,test,publish}.sh | xargs -r -n1 bash`
to rebuild and re-publish the image in a case of a loss of it,
without the need for bootstrapping anymore.
